

$(function () {
var socket = io();
var notify;

 var camp = io.connect("/camp");
 var comment = io.connect("/comment");

 comment.on('comment', function(data){
  if(Notification.permission === "default"){
    alert("please allow notifications");
  } else if(Notification.permission === "granted"){
      notify = new Notification("New Comment", {
               body: data.message,
               icon: data.image
            });
        }
//console.log("Data: " + camp);
      
notify.onclick = function(){
          window.location = '/campgrounds/' + this.tag; 
      }

setTimeout(notify.close.bind(notify), 2000);
//alert("Data: " + data + "\nStatus: " + status);


 })

camp.on('camp',function(data) {

            if(Notification.permission === "default"){
              alert("please allow notifications");
            } else if(Notification.permission === "granted"){
                updateNotificationCount();
                notify = new Notification("New Campground", {
                         body: data.message,
                         icon: data.image
                      });
                  }
        //console.log("Data: " + camp);
                
        notify.onclick = function(){
                    window.location = '/campgrounds/' + this.tag; 
                }

        setTimeout(notify.close.bind(notify), 2000);
          //alert("Data: " + data + "\nStatus: " + status);
    
});

    // Click on notification icon for show notification
    $('span.noti').click(function (e) {
        e.stopPropagation();
        $('.noti-content').show();
        var count = 0;
        count = parseInt($('span.count').html()) || 0;
        //only load notification if not already loaded
            updateNotification();
        $('span.count', this).html('&nbsp;');
    })
    // hide notifications
    $('html').click(function () {
        $('.noti-content').hide();
    })
    // update notification
    function updateNotification() {
        $('#notiContent').empty();
        $('#notiContent').append($('<li>Loading...</li>'));
        $.ajax({
            type: 'GET',
            url: u_id._id + '/notification',
            success: function (response) {
                $('#notiContent').empty();
                if (response.length  == 0) {
                    $('#notiContent').append($('<li>No data available</li>'));
                }
                $.each(response.notifications, function (index, value) {
                         $('#notiContent').append($('<li>'+ value.message +'</li>'));   
                });
                $.each(response, function(index,value){
                   if(index == 'notifications'){
                       $.each(value, function(i,v){
                          $.ajax({
                              type:'POST',
                              dataType: 'json',
                              data:{category:v.category,
                                                   title:v.title,
                                                   image:v.image,
                                                    message:v.message,
                                                    read:true,
                                                    author:v.author,
                                                    createdAt:v.createdAt},
                              url:u_id._id + '/notification/' + v._id + '?_method=PUT',
                              success: function(data){
                                  //console.log(data);
                              },
                              error : function(error){
                                  console.log(error);
                              }
                          })
                       })
                   }
                });
            },
            error: function (error) {
                console.log(error);
            }
        })
    }


    // update notification count
   function updateNotificationCount() {
    var count = 0;
    count = parseInt($('span.count').html()) || 0;
    $.ajax({
        type: 'GET',
        url: u_id._id + '/notification',
        success: function (response) {
            $('#notiContent').empty();
            if (response.length  == 0) {
                $('#notiContent').append($('<li>No data available</li>'));
            }
            $.each(response.notifications, function (index, value) {
                     if(!value.read){
                        count++;
                        $('span.count').html(count);
                     }  
            });
        },
        error: function (error) {
            console.log(error);
        }
    })
       
    };

    updateNotificationCount();
});
