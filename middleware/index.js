var middlewareobj = {};
var Campground = require("../models/campground");
var Comment = require("../models/comment");

middlewareobj.checkCampgroundsOwnership = function(req,res,next) {
     if(req.isAuthenticated()){
        Campground.findById(req.params.id,function(err,foundCampground){
             if(err){
                    req.flash("error","Campground not found");
                    res.redirect("back");
                } else{
                    if(foundCampground.author.id.equals(req.user._id)){
                       next();
                    } else{
                        req.flash("error","you do not have permission to do that");
                        res.redirect("back");
                    }
                }
        });    
    } else {
        req.flash("error", "You need to be logged in to do that");
        res.redirect("back");
    }
    
}

middlewareobj.checkCommentOwnership = function(req,res,next){
     if(req.isAuthenticated()){
        Comment.findById(req.params.comment_id, function(err, foundComment) {
            if(err){
                req.flash("error","You don't have permission to do that");
                res.redirect("back");
            } else{
                if(foundComment.author.id.equals(req.user._id)){
                    next();
                } else {
                    res.redirect("back");
                }
            }
        })
    } else {
        req.flash("error","you need to be logged in");
        res.redirect("back");
    }
}


middlewareobj.isLoggedIn = function(req,res,next){
    if(req.isAuthenticated()){
        return next();
    }
    req.flash("error","Please Login to the system first");
    res.redirect("/login");
}

middlewareobj.isAdmin = function(req, res, next) {
    if(req.user.isAdmin) {
      next();
    } else {
      req.flash('error', 'This site is now read only thanks to spam and trolls.');
      res.redirect('back');
    }
  };
  
middlewareobj. isSafe = function(req, res, next) {
    if(req.body.image.match(/^https:\/\/images\.unsplash\.com\/.*/)) {
      next();
    }else {
      req.flash('error', 'Only images from images.unsplash.com allowed.\nSee https://youtu.be/Bn3weNRQRDE for how to copy image urls from unsplash.');
      res.redirect('back');
    }
};

middlewareobj. camp_noti = function(req, res, result){
           if(result.category = 'campground'){
               req.app.campgroundNotification.emit('camp', result);
           } else if (result.category = 'comment'){
                req.app.commentNotification.emit('comment', result);
           }
};


module.exports=middlewareobj;