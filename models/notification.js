var mongoose = require("mongoose");

// the notification schema have a subscribers to specific notification (objectId)
//
const NotificationSchema = new mongoose.Schema({

  category : {
    type: String
  },  
  title: {
    type: String
  },
  image:{
    type: String
  },
  message: {
    type: String
  },
  read: {
    type: Boolean,
    default: false
  },
  author: {
    id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"User"
    },
    username: String
},
  createdAt: {
    type: Date,
    default: Date.now()
  }
});


 

module.exports = mongoose.model("Notification", NotificationSchema);