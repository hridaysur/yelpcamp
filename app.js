var express = require("express"),
    app = express(),
    http=require("http").Server(app),
    bodyParser = require("body-parser"),
    mongoose = require("mongoose"),
    Campground = require("./models/campground"),
    Comment = require("./models/comment"),
    flash=require("connect-flash"),
    User = require("./models/user"),
    Notification = require('./models/notification'),
    methodOverride=require("method-override"),
    seedDB = require("./seeds"),
    passport = require("passport"),
    cookieParser = require("cookie-parser"),
    jQuery = require("jquery"),
    LocalStrategy = require("passport-local"),
    io = require('socket.io')(http)
    

//configure dotenv
require('dotenv').load();

var commentRoutes=require("./routes/comments"),
    campgroundRoutes=require("./routes/campgrounds"),
    indexRoutes=require("./routes/index"),
    notificationRoutes=require('./routes/notification')
    
app.set("socketio",io);
mongoose.Promise = global.Promise;
//const databaseUri = 'mongodb://localhost/yelp_camp_v2';
const databaseUri=process.env.DATABASEURL;
const PORT = process.env.PORT || 4000;
//const databaseUri = 'mongodb://hriday1207:Nisha1207@ds119650.mlab.com:19650/yelp_camp_v1'

mongoose.connect(databaseUri)
      .then(() => console.log(`Database connected`))
      .catch(err => console.log(`Database connection error: ${err.message}`));


app.use(bodyParser.urlencoded({extended: true}));

app.set("view engine", "ejs");

app.use(express.static(__dirname + "/public"));
//seedDB();
app.use(cookieParser('secret'));

app.locals.moment = require('moment');

app.use(flash());
//PASSPORT SETUP
app.use(require("express-session")({
    secret:"I am Hriday",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function(req, res, next){
    res.locals.currentUser=req.user;
    res.io = io;
    res.locals.message_error=JSON.stringify(req.flash("error"));
    res.locals.message_success=JSON.stringify(req.flash("success"));
    next();
})


app.use(methodOverride("_method"));
app.use("/",indexRoutes);
app.use("/campgrounds/:id/comments",commentRoutes);
app.use("/campgrounds",campgroundRoutes);
app.use('/:user_id/notification',notificationRoutes);

var commentNotification = io.of('/comment');
var campgroundNotification = io.of('/camp');

commentNotification.on('connection',function(socket){
               //console.log('someone connected');
           })


app.campgroundNotification = campgroundNotification;
app.commentNotification = commentNotification;



http.listen(PORT, function(){
    console.log("The yelp Camp server has started");
})