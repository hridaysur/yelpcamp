What is this repository for?
YelpCamp is a place where user can see campgrounds, upload there favourite campgrounds

How do I get set up?

NPM and Node.js installed,
User can installed all the dependecies through npm

"dependencies": {
    "body-parser": "^1.18.2",
    "connect-flash": "^0.1.1",
    "cookie-parser": "^1.4.3",
    "dotenv": "^5.0.1",
    "ejs": "^2.5.7",
    "events": "^2.0.0",
    "express": "^4.16.2",
    "express-session": "^1.15.6",
    "geocoder": "^0.2.3",
    "growly": "^1.3.0",
    "jquery": "^3.3.1",
    "method-override": "^2.3.10",
    "moment": "^2.22.1",
    "mongodb-topology-manager": "^2.0.0",
    "mongoose": "^5.0.9",
    "node-notifier": "^5.2.1",
    "passport": "^0.4.0",
    "passport-local": "^1.0.0",
    "passport-local-mongoose": "^5.0.0",
    "push-notifications": "^1.0.1",
    "require": "^2.4.20",
    "socket.io": "^2.1.0"
  }

  ex. npm install socket.io --save

Database configuration

use mongoose framework for MongoDB.
save your database connection string in to a PROCESS.env.MongoURI
connect using mongoose

Who do I talk to?
created by Hriday Sur
email- hridaysur@yahoo.co.in