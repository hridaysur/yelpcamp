
var express=require("express");
var router = express.Router({mergeParams: true});
var middleware = require("../middleware");
var notification = require("../models/notification");
var user = require("../models/user");

router.get('/',middleware.isLoggedIn , (req, res) => {
    user.findById(req.params.user_id).populate('notifications')
    .exec().then(result =>
        res.status(200).json(result))
        .catch(err =>  res.status(404).json(err))
})

router.put("/:notification_id",middleware.isAdmin,function(req,res){
    notification.findByIdAndUpdate(req.params.notification_id,req.body,function(err,updateNotification){
        if(err){
            res.status(404).json(err);
        } else {
            res.status(200).json(updateNotification);
        }
    });
});

module.exports = router;