
var express=require("express");
var router = express.Router({mergeParams: true});
var Campground = require("../models/campground");
var Comment = require("../models/comment");
var Notification = require("../models/notification")
var middleware = require("../middleware");

//--------------------------------
//Comments Routes
//--------------------------------

router.get("/new", middleware.isLoggedIn, function(req, res) {
    Campground.findById(req.params.id, function(err, camp){
        if(err){
            console.log(err);
        } else {
            res.render("comments/new",{camp:camp});
        }
    })
})

router.post("/", middleware.isLoggedIn, function(req, res){
    Campground.findById(req.params.id,function(err, camp) {
        if(err){
            console.log(err);
        } else {
            Comment.create(req.body.comment,function(err, newComment){
                if(err){
                    req.flash("error","something went wrong");
                    console.log(err);
                } else {
                    //add user name and id
                    newComment.author.id = req.user._id;
                    newComment.author.username = req.user.username;
                    //save the database
                    newComment.save();
                    camp.comments.push(newComment);
                    camp.save().then((result) => {
                        middleware.camp_noti(req,res,result);
                        req.flash("success","Sucessfully added comment");
                        res.redirect("/campgrounds/"+camp._id);
                    }).catch((err) => {
                        console.log(err);
                        req.flash('error',err);
                    });
                   
                }
            })
        }
    })
})


router.get("/:comment_id/edit",middleware.isLoggedIn,middleware.checkCommentOwnership,function(req,res){
    Comment.findById(req.params.comment_id, function(err, foundComment) {
        if(err){
            res.redirect("/campgrounds");
        } else {
               res.render("comments/edit", {camp_id : req.params.id , comment : foundComment});
        }
    })
});

router.put("/:comment_id",middleware.isAdmin,function(req,res){
    Comment.findByIdAndUpdate(req.params.comment_id,req.body.comment,function(err,updateComment){
        if(err){
            res.redirect("back");
        } else {
            res.redirect("/campgrounds/" + req.params.id);
        }
    });
});

router.delete("/:comment_id",middleware.checkCommentOwnership, function(req, res){
    Comment.remove({_id:req.params.comment_id},function(err){
        if(err){
            req.flash('error',err)
        } else{
            res.redirect("/campgrounds/" + req.params.id);
        }
    }) 
});


module.exports = router;